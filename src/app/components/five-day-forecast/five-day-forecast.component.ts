import { Component, OnInit, Input } from '@angular/core';
import {WeatherServiceService} from '../../weather-service.service';

@Component({
  selector: 'app-five-day-forecast',
  templateUrl: './five-day-forecast.component.html',
  styleUrls: ['./five-day-forecast.component.scss']
})
export class FiveDayForecastComponent implements OnInit {
  @Input() zipCode: string;
  public numDays: number = 7;
  public weatherData: any;

  constructor(private weatherService: WeatherServiceService) { }


  updateData() {

    if(this.zipCode.length == 5){
      this.weatherService.getWeatherData(this.zipCode, this.numDays).then(response => {
        return response.json();
      }).then(data => {
        this.weatherData = data;
        console.log(data);
      });
    }
  }

  ngOnInit() {
    this.updateData();
  }

}

