import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kelvinToFahrenheit'
})
export class KelvinToFahrenheitPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    return Math.ceil((value * 9/5) - 459.67);
  }

}
